import { POST_LIST } from './../../../services/post.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {
  public datapost = POST_LIST;
  public post;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.getPostDetail();
  }

  getPostDetail() {
    let id;
    this.route.paramMap.subscribe(data => {
      id = data.get('id');
    });
     this.datapost.filter(x => {
      if (x.id == id) {
        this.post = x;
      }
    });
    console.log(this.post);

  }

}

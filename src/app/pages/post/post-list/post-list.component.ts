import { POST_LIST } from './../../../services/post.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  public posts = POST_LIST;
  constructor() { }

  ngOnInit() {
  }

}

import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

export const POST_LIST = [
  { id: 1, title: 'learn angular 9', content: 'hello angular' },
  { id: 2, title: 'learn typescript', content: 'hello typescript' },
  { id: 3, title: 'learn javascript', content: 'hello javascript' },
  { id: 4, title: 'learn scss', content: 'hello scss' },
];

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient
  ) { }

}
